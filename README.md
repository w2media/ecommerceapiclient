W2media Ecommerce API Client library
====================================

Co knihovna umožňuje
---
- načítání seznamu produktů a stránkování výsledku
- načítání detailu produktu podle jeho ID
- načtení kompletní stromové struktury kategorií
- načtení detailu vybrané kategorie podle ID, včetně jejích podkategorií

Ověření uživatele
---
API používá pro autentifikaci uživatelů mechanismus OAuth2. Každá klientská aplikace má přidělenou dvojici klíčů client-id/secret. 
Tato dvoji klíčů pak slouží k ověření klientské aplikace, je unikátní pro každého zákazníka a proto je třeba dbát na bezpečnost a ochranu před zcizením. 


Instalace knihovny
---
Knihovnu je možno instalovat pomocí Composeru. V prázdném adresáři vytvořte `composer.json`

```
{
	"name": "W2media Ecommerce API client",
	"require": {
		"adoy/oauth2": "dev-master",
		"w2media/ecommerceapiclient": "1.*"	
	},
	"repositories": [{
		"type": "vcs",
		"url": "git@bitbucket.org:w2media/ecommerceapiclient.git"		
	}]
}
```

Instalaci provedete pomocí příkazu:
> php composer.phar install


Příklady užití
---
Příklady, jak knihovnu použít naleznete ve složce [vendor/w2media/ecommerceapiclient/example](https://bitbucket.org/w2media/ecommerceapiclient/src/master/example/)


Autor a kontakt
---
Petr Sedláček
    - pesu@w2media.cz