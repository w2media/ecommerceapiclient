<?php

namespace W2\Ecommerce\Api\Client\TokenStorage;

use Memcache;

/**
 *
 * @author pesu
 */
class MemcacheStorage implements TokenStorageInterface {
	
	const KEY = 'w2_api.authentication_tokens';
	
	/** @var Memcache */
	private $memcache;





	/**
	 * 
	 * @param string $host
	 */
	public function __construct($host = '127.0.0.1', $port = 11211) {
		$this->memcache = new Memcache();
		$this->memcache->connect($host, $port);
	}





	/**
	 * 
	 * @param array $auth
	 */
	public function load() {
		$json = $this->memcache->get(self::KEY);
		
		if ($json === NULL) {
			return;
		}
		
		return json_decode($json, TRUE);
	}





	/**
	 * 
	 * @param array $auth
	 */
	public function save($auth) {
		$auth['expires_at'] = time() + $auth['expires_in'];

		$this->memcache->set(self::KEY, json_encode($auth));
	}
}
