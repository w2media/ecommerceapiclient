<?php

namespace W2\Ecommerce\Api\Client\TokenStorage;

/**
 *
 * @author pesu
 */
class FilesystemStorage implements TokenStorageInterface {





	/**
	 * 
	 * @param array $auth
	 */
	public function load() {
		if (!file_exists("./auth.json")) {
			return;
		}

		return json_decode(file_get_contents("./auth.json"), TRUE);
	}





	/**
	 * 
	 * @param array $auth
	 */
	public function save($auth) {
		$auth['expires_at'] = time() + $auth['expires_in'];

		file_put_contents("./auth.json", json_encode($auth));
	}
}
