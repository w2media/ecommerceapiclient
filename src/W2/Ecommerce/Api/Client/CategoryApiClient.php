<?php

namespace W2\Ecommerce\Api\Client;

/**
 * 
 * @author pesu
 */
class CategoryApiClient extends AbstractApiClient {





	/**
	 * 
	 * @return \self
	 */
	public static function create(TokenStorageInterface $tokenStorage) {
		return new self($tokenStorage);
	}





	/**
	 * 
	 * @return array
	 */
	public function getCategories() {
		return $this->get("/api/v1/categories");
	}





	/**
	 * 
	 * @param integer $id
	 * @return array
	 */
	public function getCategory($id) {
		return $this->get("/api/v1/category/" . $id);
	}
}
