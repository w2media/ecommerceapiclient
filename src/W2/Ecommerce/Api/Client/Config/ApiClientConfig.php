<?php

namespace W2\Ecommerce\Api\Client\Config;

/**
 * 
 * @author pesu
 */
class ApiClientConfig implements ApiClientConfigInterface {

	/** @var string $host */
	protected $host;

	/** @var string $h$clientIdost */
	protected $clientId;

	/** @var string $clientSecret */
	protected $clientSecret;





	/**
	 * 
	 * @param string $host
	 * @param string $clientId
	 * @param string $clientSecret
	 */
	public function __construct($host, $clientId, $clientSecret) {
		$this->host = $host;
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
	}





	/**
	 * 
	 * @return string
	 */
	public function getHost() {
		return $this->host;
	}





	/**
	 * 
	 * @param string $host
	 * @return \W2\Ecommerce\Api\Client\Config\ApiClientConfig
	 */
	public function setHost($host) {
		$this->host = $host;
		return $this;
	}





	/**
	 * 
	 * @return string
	 */
	public function getClientId() {
		return $this->clientId;
	}





	/**
	 * 
	 * @param string $clientId
	 * @return ApiClientConfig
	 */
	public function setClientId($clientId) {
		$this->clientId = $clientId;
		return $this;
	}





	/**
	 * 
	 * @return string
	 */
	public function getClientSecret() {
		return $this->clientSecret;
	}





	/**
	 * 
	 * @param string $clientSecret
	 * @return ApiClientConfig
	 */
	public function setClientSecret($clientSecret) {
		$this->clientSecret = $clientSecret;
		return $this;
	}
}
