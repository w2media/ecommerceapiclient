<?php

namespace W2\Ecommerce\Api\Client;

/**
 * 
 * @author pesu
 */
class ProductApiClient extends AbstractApiClient {





	/**
	 * 
	 * @return \self
	 */
	public static function create(TokenStorageInterface $tokenStorage) {
		return new self($tokenStorage);
	}





	/**
	 * 
	 * @param int $start
	 * @param int $length
	 * @return array
	 */
	public function getProducts($start, $length) {
		return $this->get("/api/v1/products", array(
					'start' => $start,
					'length' => $length,
		));
	}





	/**
	 * 
	 * @param integer $id
	 * @return array
	 */
	public function getProduct($id) {
		return $this->get("/api/v1/product/" . $id);
	}
}
