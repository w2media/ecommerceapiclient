<?php

namespace W2\Ecommerce\Api\Client;

use W2\Ecommerce\Api\Exception\ApiAuthenticationException;
use W2\Ecommerce\Api\Client\TokenStorage\TokenStorageInterface;
use W2\Ecommerce\Api\Client\Config\ApiClientConfigInterface;
use OAuth2;

/**
 * 
 * @author pesu
 */
abstract class AbstractApiClient {

	/** @var OAuth2 $client */
	protected $client;

	/** @var TokenStorageInterface */
	protected $tokenStorage;

	/** @var ApiClientConfigInterface */
	private $config;

	/** @var string */
	private $tokenEndpoint = '/oauth/v2/token';

	/** @var boolean */
	private $autoRefresh = TRUE;





	/**
	 * 
	 * @param TokenStorageInterface $tokenStorage
	 */
	public function __construct(ApiClientConfigInterface $config, TokenStorageInterface $tokenStorage) {
		$this->config = $config;
		$this->tokenStorage = $tokenStorage;
	}





	/**
	 * 
	 * @param string $clientId
	 * @return \ApiClient
	 */
	public function setClientId($clientId) {
		$this->clientId = $clientId;
		return $this;
	}





	/**
	 * 
	 * @param string $clientSecret
	 * @return \ApiClient
	 */
	public function setClientSecret($clientSecret) {
		$this->clientSecret = $clientSecret;
		return $this;
	}





	/**
	 * 
	 * @param TokenStorageInterface $tokenStorage
	 * @return \W2\Ecommerce\Api\Client\AbstractApiClient
	 */
	public function setTokenStorage(TokenStorageInterface $tokenStorage) {
		$this->tokenStorage = $tokenStorage;
		return $this;
	}





	/**
	 * 
	 * @param string $tokenEndpoint
	 * @return \ApiClient
	 */
	public function setTokenEndpoint($tokenEndpoint) {
		$this->tokenEndpoint = $tokenEndpoint;
		return $this;
	}





	/**
	 * 
	 * @param boolean $autoRefresh
	 * @return \W2\Ecommerce\Api\Client\AbstractApiClient
	 */
	public function setAutoRefresh($autoRefresh) {
		$this->autoRefresh = (bool) $autoRefresh;
		return $this;
	}





	/**
	 * 
	 * @return boolean
	 */
	public function isAuthenticated() {
		$auth = $this->tokenStorage->load();

		return is_array($auth) && isset($auth['access_token'], $auth['refresh_token'], $auth['expires_in']);
	}





	/**
	 * 
	 * @return boolean
	 */
	public function isExpired() {
		$auth = $this->tokenStorage->load();

		return time() > $auth['expires_at'];
	}





	/**
	 * 
	 * @param string $username
	 * @param string $password
	 */
	public function authenticate($username, $password) {
		$params = array(
			'username' => $username,
			'password' => $password,
			'scope' => 'api_oneal_products api_oneal_orders',
		);

		$response = $this->createClient()->getAccessToken($this->buildUrl($this->tokenEndpoint), OAuth2\Client::GRANT_TYPE_PASSWORD, $params);

		if ($response['code'] !== 200) {
			throw new ApiAuthenticationException($response['result']['error_description']);
		}

		$this->tokenStorage->save($response['result']);
		$this->client = NULL;
	}





	/**
	 * 
	 */
	public function refreshToken() {
		$auth = $this->tokenStorage->load();

		$params = array(
			'refresh_token' => $auth['refresh_token'],
		);

		$response = $this->createClient()->getAccessToken($this->buildUrl($this->tokenEndpoint), OAuth2\Client::GRANT_TYPE_REFRESH_TOKEN, $params);

		$this->tokenStorage->save($response['result']);
		$this->client = NULL;
	}





	/**
	 * 
	 * @param string $uri
	 * @param array $params
	 */
	public function get($uri, $params = array()) {
		return $this->getClient()->fetch($this->buildUrl($uri), $params, OAuth2\Client::HTTP_METHOD_GET);
	}





	/**
	 * 
	 * @param string $uri
	 * @param array $params
	 */
	public function post($uri, $params = array()) {
		return $this->getClient()->fetch($this->buildUrl($uri), $params, OAuth2\Client::HTTP_METHOD_POST);
	}





	/**
	 * 
	 * @param string $uri
	 * @param array $params
	 */
	public function put($uri, $params = array()) {
		return $this->getClient()->fetch($this->buildUrl($uri), $params, OAuth2\Client::HTTP_METHOD_PUT);
	}





	/**
	 * 
	 * @return OAuth2
	 * @throws ApiAuthenticationException
	 */
	private function getClient() {
		if (!$this->isAuthenticated()) {
			throw new ApiAuthenticationException("UsernamePassword authentication is necessary");
		}

		if ($this->isExpired()) {
			if ($this->autoRefresh === TRUE) {
				$this->refreshToken();
			} else {
				throw new ApiAuthenticationException("AccessToken has expired, auto refresh is disabled");
			}
		}

		if ($this->client !== NULL) {
			return $this->client;
		}

		$auth = $this->tokenStorage->load();

		$this->client = $this->createClient();
		$this->client->setAccessTokenType(OAuth2\Client::ACCESS_TOKEN_BEARER);
		$this->client->setAccessToken($auth['access_token']);

		return $this->client;
	}





	/**
	 * 
	 * @param string $uri
	 * @return string
	 */
	private function buildUrl($uri) {
		return sprintf("https://%s%s", $this->config->getHost(), $uri);
	}





	/**
	 * 
	 * @return \OAuth2\Client
	 */
	private function createClient() {
		return new OAuth2\Client($this->config->getClientId(), $this->config->getClientSecret());
	}
}
