<?php

namespace W2\Ecommerce\Api\Client;

/**
 * 
 * @author pesu
 */
class OrderApiClient extends AbstractApiClient {





	/**
	 * 
	 * @return \self
	 */
	public static function create(TokenStorageInterface $tokenStorage) {
		return new self($tokenStorage);
	}





	/**
	 * 
	 * @param int $start
	 * @param int $length
	 * @return array
	 */
	public function getOrders($start, $length) {
		return $this->get("/api/v1/orders", array(
					'start' => $start,
					'length' => $length,
		));
	}





	/**
	 * 
	 * @param integer $id
	 * @return array
	 */
	public function getOrder($id) {
		return $this->get("/api/v1/order/" . $id);
	}
}
