<?php

require "config.php";

$client = new W2\Ecommerce\Api\Client\OrderApiClient($config, $storage);

if (!$client->isAuthenticated()) {
	try {
		$client->authenticate($username, $password);
	} catch (W2\Ecommerce\Api\Exception\ApiAuthenticationException $e) {
		echo $e->getMessage();
		exit;
	}
}

$list = $client->getOrders(0, 14);
var_dump($list);