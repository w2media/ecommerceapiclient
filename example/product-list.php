<?php

require "config.php";

$client = new W2\Ecommerce\Api\Client\ProductApiClient($config, $storage);

if (!$client->isAuthenticated()) {
	try {
		$client->authenticate($username, $password);
	} catch (W2\Ecommerce\Api\Exception\ApiAuthenticationException $e) {
		echo $e->getMessage();
		exit;
	}
}

$list = $client->getProducts(0, 14);
var_dump($list);