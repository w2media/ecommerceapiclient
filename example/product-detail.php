<?php

require "config.php";

$client = new W2\Ecommerce\Api\Client\ProductApiClient($config, $storage);

if (!$client->isAuthenticated()) {
	try {
		$client->authenticate($username, $password);
	} catch (W2\Ecommerce\Api\Exception\ApiAuthenticationException $e) {
		echo $e->getMessage();
		exit;
	}
}

$product = $client->getProduct(1534);
var_dump($product);
