<?php

require "config.php";

$client = new W2\Ecommerce\Api\Client\OrderApiClient($config, $storage);

if (!$client->isAuthenticated()) {
	try {
		$client->authenticate($username, $password);
	} catch (W2\Ecommerce\Api\Exception\ApiAuthenticationException $e) {
		echo $e->getMessage();
		exit;
	}
}

$order = $client->getOrder(9999);
var_dump($order);
