<?php

require "config.php";

$client = new W2\Ecommerce\Api\Client\CategoryApiClient($config, $storage);

if (!$client->isAuthenticated()) {
	try {
		$client->authenticate($username, $password);
	} catch (W2\Ecommerce\Api\Exception\ApiAuthenticationException $e) {
		echo $e->getMessage();
		exit;
	}
}

$category = $client->getCategory(705);
var_dump($category);
