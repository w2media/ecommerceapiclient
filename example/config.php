<?php

require "./vendor/autoload.php";

$username		= 'xxx';	// uživatelské jméno přihlašujícího se uživatele
$password		= '***';	// heslo

$host			= 'sandbox.oneal.cz';	// vývojové prostředí
//$host			= 'www.oneal.cz';		// produkční prostředí

$clientId		= 'aaa';	// přidělené client_id
$clientSecret	= 'bbb';	// přidělené client_secret


// nejprve vytvoříme konfigurační objekt
// můžeme použít připravený nebo použít vlastní, který
// implementuje ApiClientConfigInterface
$config = new W2\Ecommerce\Api\Client\Config\ApiClientConfig($host, $clientId, $clientSecret);


// vytvoříme storage pro získaný AccessToken
// knihovna nabízí std. dva, MemcacheStorage a FilesystemStorage
// je možné použít jakýkoliv vlastní, který musí pouze implementovat 
// interface TokenStorageInterface
$storage = new W2\Ecommerce\Api\Client\TokenStorage\MemcacheStorage();
